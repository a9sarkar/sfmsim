package com.sfm.constants;

public class GeneralConst {
	/**
	 * threshold for an agent to be considered "at" their destination
	 */
	public static double distanceThresh = 0.5;
	
	public static int gridWidth = 19;
	public static int gridHeight = 10;
	public static int gridDepth = 3;
	public static int gridCentreX = 0;
	public static int gridCentreY = 9; //zero based
	public static double gridDim = 0.5; //metres per cell
	public static int terminate = 3;
	public static int normal = 2;
}
