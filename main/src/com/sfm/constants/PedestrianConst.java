package com.sfm.constants;

import com.sfm.ds.NormalDistribution;

public class PedestrianConst {
	/*private static Hashtable<Long,PedestrianConst> instances = new Hashtable<Long,PedestrianConst>();
	
	private PedestrianConst()
	{
		
	}
	
	public static static synchronized PedestrianConst getInst()
	{
		long id = Thread.currentThread().getId();
		PedestrianConst ret;
		
		if(!instances.containsKey(id))
		{
			ret = new PedestrianConst();
			instances.put(id, ret);
		}
		else
			ret = instances.get(id);
		
		return ret;	
	}
	
	public static void updateConst(List<Object> values)
	{
		if(values.size() != 11)
			throw new RuntimeException("Wrong number of const");
		
		evasiveSpeedChange = (NormalDistribution) values.get(0);
		evasiveAngleChange =  (NormalDistribution) values.get(1);
		evasiveRelativeTimeThresh = (double) values.get(2);
		desiredSpeed =  (NormalDistribution) values.get(3);
		accelerationTime = (NormalDistribution) values.get(4);
		repulsiveStrengthCoefA = (double) values.get(5);
		repulsiveStrengthCoefB = (double) values.get(6);
		crosswalkPullStrengthCoefA = (double) values.get(7);
		crosswalkPullStrengthCoefB = (double) values.get(8);
		crosswalkPushStrengthCoefA = (double) values.get(9);
		crosswalkPushStrengthCoefB = (double) values.get(10);
	}*/
	
	/* *************requires calibration****************/
	/* *****observable********/
	/**
	 * distribution of the change in magnitude of velocity when pedestrian is evading other pedestrians
	 */
	public static NormalDistribution evasiveSpeedChange = new NormalDistribution(0.0, 1.0);
	/**
	 * distribution of the change in angle (degrees) of the velocity when a pedestrian is evading other pedestrians
	 */
	public static NormalDistribution evasiveAngleChange = new NormalDistribution(0.0,90.0);
	/**
	 * distribution of the desired speed for pedestrians
	 */
	public static NormalDistribution desiredSpeed = new NormalDistribution(1.6,0.15);
	/**
	 * distribution of this acceleration time for pedestrians
	 */
	public static NormalDistribution accelerationTime = new NormalDistribution(0.5,0.3);
	/* ****not observable****/
	/**
	 * coefficient in repulsive equation
	 */
	public static double repulsiveStrengthCoefA = 0.85;
	/**
	 * coefficient in repulsive equation
	 */
	public static double repulsiveStrengthCoefB = 0.76;
	public static double crosswalkPullStrengthCoefA = 0.42;
	public static double crosswalkPullStrengthCoefB = 0.95;
	public static double crosswalkPushStrengthCoefA = 0.21;
	public static double crosswalkPushStrengthCoefB = 0.84;
	
	/* ************doesn't require calibration**********/
	/**
	 * threshold for the relative time to conflict point to continue generating evasive maneuvers
	 */
	public static double evasiveRelativeTimeThresh = 2.0;
	/**
	 * range for pedestrians to be repelled by others
	 */
	public static double visualRange = 5.0;
	/**
	 * angle off pedestrians direction for others to affect it
	 */
	public static double fieldOfView = 0.5*170.0*Math.PI/180.0;
	/**
	 * range for pedestrians to start evasive maneuvers
	 */
	public static double privateSphere = 3.0;
	/**
	 * radius of pedestrian (for drawing and intersection)
	 */
	public static double radius = 0.5;
}
