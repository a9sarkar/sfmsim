package com.sfm.constants;

public class EgoCarConst {
	public static final double drivingForce = 0.3;
	public static final double stoppingForce = -0.3;
	public static final double maxX = 25.0;
	public static final double carWidth = 1.8;
	public static final double carLength = 4.8;
	public static final boolean includeCar = true;
}
