package com.sfm.engine;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.apache.commons.math3.ode.nonstiff.EulerIntegrator;

import com.sfm.BaseMap;
import com.sfm.Env;
import com.sfm.agents.Agent;
import com.sfm.agents.AgentContainer;
import com.sfm.agents.AgentType;
import com.sfm.agents.EgoCar;
import com.sfm.agents.Pedestrian;
import com.sfm.agents.factories.SceneFactory;
import com.sfm.constants.EgoCarConst;
import com.sfm.constants.GeneralConst;
import com.sfm.ds.Vector2d;
import com.sfm.helper.SimulationPlayer;
import com.sfm.helper.SimulationSnapshot;

public class SFMEngine {
	
	Env environment;
	MotionODE motion;
	FirstOrderIntegrator integrator;
	
	double curTime;
	int outTime;
	double timeStep;
	
	public ArrayList<SimulationSnapshot> snapshots = new ArrayList<SimulationSnapshot>();
	private double timethresh = 100;
	
	public SFMEngine(double timeStep, AgentContainer agents, BaseMap map)
	{
		environment = new Env(agents,map);
		motion = new MotionODE(environment);
		this.timeStep = timeStep;
		curTime = 0.0;
		environment.getAgents().car.setGoing(true);
		//TODO: use better integrator
		integrator = new ClassicalRungeKuttaIntegrator(0.01);
	}
	
	public SFMEngine(double timeStep, int numPed, String mapName)
	{
		this(timeStep, numPed, new BaseMap(mapName));
	}
	
	public SFMEngine(double timeStep, int numPed, BaseMap map)
	{
		this(timeStep, SceneFactory.generateScene(numPed, map), map);
	}

	public double updateAndStep(boolean go)
	{
		environment.getAgents().car.setGoing(go);
		tick();
		return environment.getReward();
	}

	public void tick()
	{
		//int temp = (int)(curTime/timeStep);
		/*if((int)(curTime/timeStep) % 50 == 0)
		{
			environment.getAgents().car.toggleGoing();
		}
		/*if((int)(curTime/timeStep) % 222 == 0)
		{
			int a = 0;
		}*/
		double[] y = new double[environment.getAgents().primitiveLength()];
		integrator.integrate(motion, curTime, environment.getAgents().toPrimitiveList(), curTime+timeStep, y);
		for(int i = 0; i < y.length; i ++)
		{
			if(Double.isNaN(y[i]))
			{
				//System.out.println("GOT NAN");
			}
		}
		environment.getAgents().UpdateFromPrimitiveList(y);
		
		curTime += timeStep;
		
		if((int)curTime != outTime)
		{
			outTime = (int) curTime;
			//System.out.println(outTime);
		}
		
		//debug info
		//System.out.println("Time: " + curTime);
		SimulationSnapshot snap = new SimulationSnapshot();
		for(int i = 0; i < environment.getAgents().getInternalList().size(); i ++)
		{
			Agent cur = environment.getAgents().getInternalList().get(i);
			//System.out.println(cur.getState().position.toString());
			snap.add(new Vector2d(cur.getState().position.x,cur.getState().position.y),cur.getType(),getState()[0]);
			//System.out.println(cur.getState().position);
		}
		snapshots.add(snap);
		//System.out.println(environment.getAgents().car.getState().position.x/EgoCarConst.maxX + "%");
		//System.out.println("----------------------------------------");
	}
	
	public void run(boolean display)
	{
		//System.out.println("Thread " + Thread.currentThread().getId() + " running");
		while(!Null())
		{
			tick();
			//System.out.println("Thread " + Thread.currentThread().getId() + " tick - " + curTime);
			if(curTime > timethresh)
				break;
		}
		//System.out.println("Thread " + Thread.currentThread().getId() + " done");
		
		if(display)
		{
			display();
		}
	}
	public void display()
	{
		SimulationPlayer sp = new SimulationPlayer(snapshots,environment.map.terrain,timeStep);
		sp.run();
	}
	
	public int getLabel()
	{
		//System.out.println(Null() || environment.hit);
		return Null() || environment.hit || environment.getAgents().car.getState().position.x > EgoCarConst.maxX? GeneralConst.terminate : GeneralConst.normal;
	}
	
	public boolean getHit()
	{
		return environment.hit;
	}
	
	public double getCarpos()
	{
		//System.out.println("java carpos: " + environment.getAgents().car.getState().position.x);
		return environment.getAgents().car.getState().position.x;
	}
	
	public double[][][] getState()
	{
		double[][][] map = new double[GeneralConst.gridDepth][GeneralConst.gridWidth][GeneralConst.gridHeight];
		List<Agent> tempList = environment.getAgents().getInternalList();
		EgoCar car = environment.getAgents().car;
		
		for(int i = 0; i < tempList.size(); i++)
		{
			Agent cur = tempList.get(i);
			if(cur.getType() == AgentType.Pedestrian)
			{
				Vector2d temp = cur.getState().position.sub(car.getState().position.add(new Vector2d(EgoCarConst.carLength/4.0,0)));
				if(temp.mag() > 5.0)
					continue;
				Vector2d translated = temp.mul(1.0/GeneralConst.gridDim);
				translated.x += GeneralConst.gridCentreX;
				translated.y = GeneralConst.gridCentreY - translated.y;
				int y = (int) Math.round(translated.x);
				int x = (int) Math.round(translated.y);
				
				if(x >= 0 && x < GeneralConst.gridWidth && y >= 0 && y < GeneralConst.gridHeight)
				{
					map[0][x][y] = 1;
					map[1][x][y] = cur.getState().velocity.x;
					map[2][x][y] = cur.getState().velocity.y;
				}
			}
		}
		return map;
	}
	
	private boolean Null()
	{
		boolean allNull = true;
		for(Agent cur : environment.getAgents().getInternalList())
		{
			if(cur.getType() != AgentType.Null /*&& cur.getType() != AgentType.EgoCar*/)
				allNull = false;
		}
		return allNull;
	}

}
