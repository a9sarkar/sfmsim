package com.sfm.engine;

import java.util.List;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.EulerIntegrator;

import com.sfm.Env;
import com.sfm.agents.*;

public class MotionODE implements FirstOrderDifferentialEquations {

	private int dim;
	private Env environment;
	private double curTime;
	
	@Override
	public int getDimension() {
		return dim;
	}

	@Override
	public void computeDerivatives(double t, double[] y, double[] yDot) throws MaxCountExceededException, DimensionMismatchException
	{
		double dt =  t - curTime;
		if(dt < 0.000000001)
			return;
		curTime = t;
		environment.getAgents().UpdateFromPrimitiveList(y);
		environment.computeDerivatives(dt, yDot);
	}
	
	public MotionODE(Env environment)
	{
		dim = environment.getAgents().primitiveLength();
		this.environment = environment;
		curTime = 0;
	}

}
