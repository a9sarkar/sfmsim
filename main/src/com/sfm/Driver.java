package com.sfm;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.sfm.engine.SFMEngine;

public class Driver {

	public static void main(String[] args) {
		Options opts = getOptions();
		
		String mapName = getMapName(opts,args);
		if(mapName == null)
			return;
		
		SFMEngine eng = new SFMEngine(0.05,5,new BaseMap(mapName));
		eng.run(true);
	}
	
	private static String getMapName(Options opts, String[] args)
	{
		CommandLineParser parser = new DefaultParser();
		String mapName;
		try {
			CommandLine cmd = parser.parse(opts, args);
			if(cmd.hasOption("help"))
			{
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "Driver", opts );
				return null;
			}
			if(cmd.hasOption("map"))
				mapName = cmd.getOptionValue("map");
			else
				mapName = "map.xml";
		} catch (ParseException e) {
			System.out.println("Parsing failed.  Reason: " + e.getMessage());
			return null;
		}
		return mapName;
	}
	
	private static Options getOptions()
	{
		Option help = Option.builder("h")
				.longOpt( "help")
				.desc("print this message")
				.build();
		
		Option map = Option.builder("m")
			     .required(false)
			     .longOpt("map")
			     .desc("xml map file")
			     .hasArg()
			     .argName("file")
			     .build();
		
		Options opts = new Options();
		opts.addOption(help);
		opts.addOption(map);
		return opts;
	}

}
