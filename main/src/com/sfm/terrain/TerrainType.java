package com.sfm.terrain;

public enum TerrainType {
	Spawn,
	Crosswalk,
	Sidewalk
}
