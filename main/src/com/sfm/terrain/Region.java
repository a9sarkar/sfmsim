package com.sfm.terrain;

import java.util.ArrayList;
import java.util.List;

import com.sfm.agents.Pedestrian;
import com.sfm.ds.Vector2d;

public class Region {
	
	public Vector2d bottomLeft;
	public double width;
	public double height;
	public TerrainType type;
	
	public List<Region> connections = new ArrayList<Region>();

	public Region(Vector2d bottomLeft, double width, double height, TerrainType t) {
		this.bottomLeft = bottomLeft;
		this.width = width;
		this.height = height;
		type = t;
	}

	public boolean in(Vector2d position) {
		boolean xValid = position.x >= bottomLeft.x && position.x <= (bottomLeft.x + width);
		boolean yValid = position.y >= bottomLeft.y && position.y <= (bottomLeft.y + height);
		return xValid && yValid;
	}
	
	public Vector2d getForce(Pedestrian ped)
	{
		return new Vector2d();
	}
	
	public String toString()
	{
		return "(" + bottomLeft.x + ", " + bottomLeft.y + ", " + width + ", " + height + ")";
	}
}
