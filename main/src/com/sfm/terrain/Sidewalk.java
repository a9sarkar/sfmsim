package com.sfm.terrain;

import com.sfm.ds.Line;
import com.sfm.ds.Vector2d;

public class Sidewalk extends Crosswalk {

	public Sidewalk(Line a, Line b) {
		super(a, b);
		type = TerrainType.Sidewalk;
	}

}
