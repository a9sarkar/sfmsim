package com.sfm.terrain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

import com.sfm.agents.Pedestrian;
import com.sfm.constants.PedestrianConst;
import com.sfm.ds.Line;
import com.sfm.ds.Vector2d;

public class Crosswalk extends Region {

	public Line edgeOne;
	public Line edgeTwo;
	
	//TODO: set region members
	public Crosswalk(Line a, Line b) {
		super(new Vector2d(0.0, 0.0), 0.0, 0.0, TerrainType.Crosswalk);
		
		//TODO: change this to be fine with any two lines
		if(!((a.vertical() && b.vertical()) || (a.horizontal() && b.horizontal())))
		{
			throw new RuntimeException("crosswalk lines must be vertical or horizontal");
		}
		
		this.edgeOne = new Line(a);
		this.edgeTwo = new Line(b);
		
		setupRegion();
	}

	private void setupRegion()
	{
		ArrayList<Vector2d> points = new ArrayList<Vector2d>();
		points.add(edgeOne.start);
		points.add(edgeOne.end);
		points.add(edgeTwo.start);
		points.add(edgeTwo.end);
		
		Collections.sort(points,new Comparator<Vector2d>(){

			@Override
			public int compare(Vector2d o1, Vector2d o2) {
				if(o1.x + o1.y < o2.x + o2.y)
					return -1;
				else
					return 1;
			}
		});
		
		bottomLeft = points.get(0);
		width = points.get(3).x - bottomLeft.x;
		height = points.get(3).y - bottomLeft.y;
	}
	
	public boolean virtical() {
		return edgeOne.vertical();
	}
	
	public boolean horizontal() {
		return edgeOne.horizontal();
	}
	
	@Override
	public Vector2d getForce(Pedestrian ped)
	{
		if(in(ped.getState().position))
			return getInsideForce(ped);
		else
			return getOutsideForce(ped);
	}
	
	private Line getClosestLine(Vector2d point)
	{
		double distOne = edgeOne.start.sub(point).mag() + edgeOne.end.sub(point).mag();
		double distTwo = edgeTwo.start.sub(point).mag() + edgeTwo.end.sub(point).mag();
		if(distOne < distTwo)
			return edgeOne;
		else
			return edgeTwo;
	}
	
	private Vector2d getInsideForce(Pedestrian ped)
	{
		Line l = getClosestLine(ped.getState().position);
		double mag2 = l.start.sub(l.end).magSq();
		Vector2d w = l.end;
		Vector2d v = l.start;
		Vector2d p = ped.getState().position;
		
		double t = p.sub(v).dot(w.sub(v))/mag2;
		if(t < 0.0 || t > 1.0)
			throw new RuntimeException("something is really wrong");
		
		Vector2d projection = w.sub(v).mul(t).add(v);
		
		Vector2d norm = ped.getState().position.sub(projection).norm();
		double dist = ped.getState().position.sub(projection).mag();
		double magnitude = ped.behaviour.crosswalkPushStrengthCoefA*Math.exp(-ped.behaviour.crosswalkPushStrengthCoefB*dist);
		
		return norm.mul(magnitude);
	}
	
	private Vector2d getOutsideForce(Pedestrian ped)
	{
		Line l = getClosestLine(ped.getState().position);
		double t = ped.getState().position.sub(l.start).dot(l.end.sub(l.start));
		if(t < 0.0 || t > 1.0)
			return new Vector2d();
		
		Vector2d projection = l.end.sub(l.start).mul(t).add(l.start);
		
		Vector2d norm = ped.getState().position.sub(projection).norm();
		double dist = ped.getState().position.sub(projection).mag();
		double magnitude = ped.behaviour.crosswalkPullStrengthCoefA*Math.exp(ped.behaviour.crosswalkPullStrengthCoefB*dist);
		
		return norm.mul(magnitude);
	}
	
}
