package com.sfm;

import com.sfm.agents.Agent;
import com.sfm.agents.AgentContainer;
import com.sfm.agents.AgentType;
import com.sfm.agents.CarFactory;
import com.sfm.agents.EgoCar;
import com.sfm.agents.Pedestrian;
import com.sfm.agents.PedestrianFactory;
import com.sfm.constants.EgoCarConst;

public class Env {
	
	AgentContainer agents;
	int numPed;
	public BaseMap map;
	public boolean hit = false;
	
	public Env(AgentContainer agents, BaseMap map)
	{
		this.agents = agents;
		this.map = map;
	}
	
	public AgentContainer getAgents() {
		return agents;
	}
	
	public double getReward()
	{
		boolean oncrosswalk = false;
		for(Agent a : agents.getInternalList())
		{
			if(a == agents.car)
				continue;
			if(a.getType() == AgentType.Pedestrian && agents.car.hit((Pedestrian)a))
			{
				hit = true;
				break;
			}
		}
		
		if(hit)
			return -100.0;
		if(agents.car.getState().position.x > EgoCarConst.maxX)
			return 100.0;
		/*if(agents.car.getGoing())
			return 1;
		return -1;*/
		return 0;
	}
	
	public void toggleEgoCar()
	{
		agents.car.toggleGoing();
	}
	
	public void computeDerivatives(double dt, double[] yDot) {
		agents.computeDerivatives(dt,yDot);
	}

}
