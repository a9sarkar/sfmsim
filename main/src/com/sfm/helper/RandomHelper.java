package com.sfm.helper;

import java.util.*;

import com.sfm.constants.PedestrianConst;
import com.sfm.ds.Vector2d;
import com.sfm.terrain.Region;

//import javafx.util.Pair;

public class RandomHelper {
	
	private static Random instance;
	
	private RandomHelper()
	{
		
	}
	
	private static Random getInstance()
	{
		if(instance == null)
		{
			long mil = System.currentTimeMillis();
			instance = new Random(mil);
			System.out.println("Random seed: " + mil );
		}
		return instance;
	}
	
	public static double nextGaussian(double mu, double sigma)
	{
		Random r = getInstance();
		double gauss = r.nextGaussian();
		double ret = gauss*sigma + mu;
		return ret;
	}
	
	public static double nextGaussian(double mu, double sigma, double lowerBound, double upperBound)
	{
		double ret;
		do
		{
			ret = nextGaussian(mu,sigma);
		}while(ret < lowerBound || ret > upperBound);
		return ret;
	}
	
	public static double nextDouble(double min, double max)
	{
		return getInstance().nextDouble()*(max-min) + min;
	}
	
	public static Vector2d nextVector2d(Region spawn) {
		double toShift = PedestrianConst.radius + 0.05;
		Region temp = new Region(spawn.bottomLeft.add(toShift), spawn.width-(2*toShift), spawn.height-(2*toShift), spawn.type);
		return new Vector2d(nextDouble(temp.bottomLeft.x, temp.bottomLeft.x + temp.width),nextDouble(temp.bottomLeft.y, temp.bottomLeft.y + temp.height));
	}

	public static int nextInt(int max) {
		return getInstance().nextInt(max);
	}
}
