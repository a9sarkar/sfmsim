package com.sfm.helper;

import java.util.ArrayList;
import java.util.List;

import com.sfm.agents.AgentType;
import com.sfm.ds.Vector2d;

public class SimulationSnapshot {
	public List<Vector2d> positions;
	public List<AgentType> types;
	double[][] carVision;
	
	public SimulationSnapshot()
	{
		positions = new ArrayList<Vector2d>();
		types = new ArrayList<AgentType>();
	}

	public void add(Vector2d vec, AgentType type, double[][] state) {
		positions.add(vec);
		types.add(type);
		carVision = state;
	}
	
	public Vector2d getPos(int index)
	{
		return positions.get(index);
	}
	
	public AgentType getType(int index)
	{
		return types.get(index);
	}
	
	public double[][] getState(int index)
	{
		return carVision;
	}
}
