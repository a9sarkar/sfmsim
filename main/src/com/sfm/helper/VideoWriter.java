package com.sfm.helper;

import com.bric.qt.io.JPEGMovWriter;
import com.sfm.BaseMap;
import com.sfm.ds.Vector2d;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VideoWriter {
	
	ArrayList<ArrayList<Vector2d>> list;
	float timestep;
	BaseMap map;
	
	public VideoWriter(ArrayList<ArrayList<Vector2d>> list, BaseMap map, double timestep)
	{
		this.timestep = (float) timestep;
		int len = list.get(0).size();
		for(int i = 0; i < list.size(); i++)
		{
			if(len != list.get(i).size())
				throw new RuntimeException("list length not the same");
		}
		this.list = list;
		this.map = map;
	}
	
	public void outputFile(String path)
	{
		File f = new File(path);
		try {
			JPEGMovWriter jmw = new JPEGMovWriter(f);
			List<Color[][]> imageList = getFrames();
			for(Color[][] img : imageList)
			{
				jmw.addFrame(timestep, getImage(img), 0.9f);
			}
			jmw.close(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private BufferedImage getImage(Color[][] img)
	{
		BufferedImage bi = new BufferedImage(img.length,img[0].length,BufferedImage.TYPE_INT_RGB);
		
		for(int i = 0; i < img.length; i ++)
		{
			for(int j = 0; j < img[i].length; j++)
			{
				bi.setRGB(i, j, img[i][j].getRGB());
			}
		}
		
		return bi;
	}
	
	private List<Color[][]> getFrames()
	{
		Vector2d min = new Vector2d(Double.MAX_VALUE,Double.MAX_VALUE);
		Vector2d max = new Vector2d(Double.MIN_VALUE,Double.MIN_VALUE);
		for(int i = 0; i < list.get(0).size(); i++)
		{
			for(int j = 0; j < list.size(); j++)
			{
				Vector2d cur = list.get(j).get(i);
				if(min.x > cur.x)
					min.x = cur.x;
				if(min.y > cur.y)
					min.y = cur.y;
				
				if(max.x < cur.x)
					max.x = cur.x;
				if(max.y < cur.y)
					max.y = cur.y;
			}
		}
		
		int scale = 10;
		int radius = 1;
		
		min.x -= 2*radius;
		min.y -= 2*radius;
		
		max.x += 2*radius;
		max.y += 2*radius;
		
		min = min.mul(scale);
		max = max.mul(scale);
		
		ArrayList<Color[][]> imageList = new ArrayList<Color[][]>();
		
		int imageWidth = (int) Math.ceil(max.x - min.x);
		int imageHeight = (int) Math.ceil(max.y - min.y);
				
		ArrayList<Color> colourList = new ArrayList<Color>();
		
		//generate colours for each agent
		for(int i = 0; i < 360; i += 360/list.size())
		{
			colourList.add(Color.getHSBColor((float)i/360.0f, 0.5f, 0.5f));
		}
		
		//draw each instance in time
		for(int i = 0; i < list.get(0).size(); i++)
		{
			Color[][] img = getBackImage(imageWidth,imageHeight);
			for(int j = 0; j < list.size(); j++)
			{
				Vector2d cur = list.get(j).get(i);
				cur = cur.mul(scale);
				int x = (int) Math.round(cur.x - min.x);
				int y = (int) Math.round(cur.y - min.y);
				
				for(int x1 = x - radius; x1 <= x+ radius; x1 ++)
				{
					for(int y1 = y - radius; y1 <= y + radius; y1++)
					{
						if(x1 < img.length && x1 > 0 && y1 < img[0].length && y1 > 0)
							img[x1][y1] = colourList.get(j);
					}
				}
			}
			imageList.add(img);
		}
		
		return imageList;
	}
	
	Color[][] getBackImage(int width, int height)
	{
		Color[][] img = new Color[width][height];
		
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				img[i][j] = Color.black;
			}
		}
		
		return img;
	}
}
