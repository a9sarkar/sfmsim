package com.sfm.helper;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.sfm.agents.AgentType;
import com.sfm.constants.EgoCarConst;
import com.sfm.constants.GeneralConst;
import com.sfm.ds.Vector2d;
import com.sfm.terrain.Region;
import com.sfm.terrain.TerrainType;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.awt.image.BufferStrategy;
import java.util.Hashtable;
import java.util.List;

public class SimulationPlayer implements Runnable {
	final int WIDTH = 1280;
	final int HEIGHT = 960;
	
	JFrame frmSim;
	Canvas canvas;
	BufferStrategy bufferStrategy;
	boolean running = true;
	List<SimulationSnapshot> snapshots;
	double frameRate;
	List<Region> terrain;
	
	int currentFrame = 0;
	int totalFrames;
	
	double lastTime = -1.0;
	
	boolean playing = true;
	
	Hashtable<TerrainType, Color> colours;
	
	AffineTransform  at = new AffineTransform();
	
	double maxY = -1000;
	
	public SimulationPlayer(List<SimulationSnapshot> snapshots, List<Region> terrain, double frameRate) {
		this.snapshots = snapshots;
		this.frameRate = frameRate;
		this.terrain = terrain;
		totalFrames = snapshots.size();
		
		setMax();
		
		frmSim = new JFrame("Prototyping");
		frmSim.setResizable(false);
		frmSim.setTitle("Sim");

		JPanel panel = (JPanel) frmSim.getContentPane();
		panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		panel.setLayout(new GridLayout());
	    
	    canvas = new Canvas();
	    canvas.setIgnoreRepaint(true);

	    panel.add(canvas);

	    frmSim.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frmSim.pack();

		canvas.createBufferStrategy(2); 
	    bufferStrategy = canvas.getBufferStrategy();

	    canvas.requestFocus();
	    
	    //set up colours
	    colours = new Hashtable<TerrainType, Color>();
	    colours.put(TerrainType.Crosswalk, Color.white);
	    colours.put(TerrainType.Sidewalk, Color.gray);
	    
	    at.scale(30.0, 30.0);
	    at.translate(10.0, 10.0);
	    
	    createListeners();
	}
	
	private void setMax()
	{
		for(SimulationSnapshot snap : snapshots)
		{
			for(Vector2d v : snap.positions)
			{
				maxY = v.y > maxY ? v.y : maxY;
			}
		}
		
		for(Region r : terrain)
		{
			maxY = r.bottomLeft.y + r.height > maxY ? r.bottomLeft.y + r.height : maxY;
		}
	}

	private void createListeners()
	{
		canvas.addMouseWheelListener(new MouseWheelListener() {
			double factor = 0.1;
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int x = e.getX();
				int y = e.getY();
				
				double scale = 1.0 - (e.getWheelRotation() < 0 ? -factor : factor);
				at.scale(scale, scale);
			}
			
		});
		
		canvas.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getY() > HEIGHT - 20)
				{
					currentFrame = Math.round(((float)e.getX()/WIDTH)*totalFrames);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
		});
		
		canvas.addMouseMotionListener(new MouseMotionListener(){
			int x = -1;
			int y = -1;
			double factor = 0.5;
			@Override
			public void mouseDragged(MouseEvent e) {
				if(x < 0 || y < 0)
				{
					x = e.getX();
					y = e.getY();
				}
				else
				{
					double dx = (e.getX() - x)*factor;
					double dy = (y - e.getY())*factor;
					x = e.getX();
					y = e.getY();
					at.translate(dx/at.getScaleX(), dy/at.getScaleY());
				}
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				x = e.getX();
				y = e.getY();
			}
			
		});
		
		canvas.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
				switch(e.getKeyCode())
				{
				case 37: //left arrow
					currentFrame--;
					if(currentFrame < 0)
						currentFrame = 0;
					break;
				case 39: //right arrow
					currentFrame++;
					if(currentFrame >= totalFrames)
						currentFrame = totalFrames-1;
					break;
				case 32: //space
					playing = !playing;
					break;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				
			}

			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
		});
	}
	
	@Override
	public void run() {
	    frmSim.setVisible(true);
		running = true;
		while(running)
		{
			render();
		}
	}
	
	private void render()
	{
	      Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();
	      g.scale(1, -1); //flip y axis so (0,0) is bottom left
          g.translate(0, -HEIGHT);
	      g.clearRect(0, 0, WIDTH, HEIGHT);
	      renderBackground(g);
	      render(g);
	      renderUI(g);
	      g.dispose();
	      bufferStrategy.show();
	}


	private void renderBackground(Graphics2D g)
	{
		g.setColor(Color.black);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		for(int i = 0; i < terrain.size(); i++)
		{
			Region cur = terrain.get(i);
			
			Double r = new Rectangle2D.Double(cur.bottomLeft.x, cur.bottomLeft.y, cur.width, cur.height);
			Shape transformed = at.createTransformedShape(r);
			g.setColor(colours.get(cur.type));
			if(cur.type != TerrainType.Spawn)
				g.fill(transformed);
			else
				g.draw(transformed);
		}
	}

	private void render(Graphics2D g) {
		setFrame();
		
		SimulationSnapshot snap = snapshots.get(currentFrame);
		
		int max = snap.positions.size() + 1;
		
		for(int i = 0; i < snap.positions.size(); i++)
		{
			Vector2d cur = snap.getPos(i);
			if(cur == null)
				continue;
			
			float h = ((float)i/(float)max);
			Color curColour = Color.getHSBColor(h, 0.7f, 0.7f);
			Shape transformed;
			if(snap.getType(i) == AgentType.Pedestrian)
			{
				Ellipse2D.Double e = new Ellipse2D.Double(cur.x, cur.y, 0.5, 0.5);
				transformed = at.createTransformedShape(e);
			}
			else if(snap.getType(i) == AgentType.EgoCar)
			{
				Rectangle2D.Double e = new Rectangle2D.Double(cur.x - (EgoCarConst.carLength/2.0),
															  cur.y - (EgoCarConst.carWidth/2.0),
															  EgoCarConst.carLength,
															  EgoCarConst.carWidth);
				transformed = at.createTransformedShape(e);
			}
			else
			{
				continue;
			}
			g.setColor(curColour);
			g.fill(transformed);
		}
		
		Vector2d base = new Vector2d(0,maxY + 1);
		
		g.setColor(Color.white);
		
		//outline grid
		for(int i = 0; i < GeneralConst.gridWidth + 1; i++)
		{
			Line2D.Double l = new Line2D.Double(i*GeneralConst.gridDim + base.x, base.y, i*GeneralConst.gridDim + base.x, GeneralConst.gridHeight*GeneralConst.gridDim + base.y);
			g.draw(at.createTransformedShape(l));
		}
		for(int i = 0; i < GeneralConst.gridHeight + 1; i++)
		{
			Line2D.Double l = new Line2D.Double(base.x,i*GeneralConst.gridDim + base.y,GeneralConst.gridWidth*GeneralConst.gridDim + base.x, i*GeneralConst.gridDim + base.y);
			g.draw(at.createTransformedShape(l));
		}
		
		//fill squares
		for(int i = 0; i < GeneralConst.gridWidth; i++)
		{
			for(int j = 0; j < GeneralConst.gridHeight; j++)
			{
				if(snap.carVision[i][j] != 0.0)
				{
					Rectangle2D.Double r = new Rectangle2D.Double(base.x + GeneralConst.gridDim*i, base.y + GeneralConst.gridDim*j, GeneralConst.gridDim, GeneralConst.gridDim);
					g.fill(at.createTransformedShape(r));
				}
			}
		}
		
		g.setColor(Color.blue);
		Rectangle2D.Double r = new Rectangle2D.Double(base.x + GeneralConst.gridDim*GeneralConst.gridCentreY, base.y + GeneralConst.gridDim*GeneralConst.gridCentreX, GeneralConst.gridDim, GeneralConst.gridDim);
		g.fill(at.createTransformedShape(r));
	}
	
	private void renderUI(Graphics2D g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, WIDTH, 20);
		
		g.setColor(Color.green);
		g.fillRect(0, 0, Math.round((float) WIDTH * (float) currentFrame/(float) totalFrames), 20);
		
		g.setColor(Color.black);
		if(playing)
		{
			g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 25));
			g.drawString("\u25B6", (float)WIDTH/2.0f, 20.0f);
		}
		else
		{
			g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 15));
			g.drawString("||", (float)WIDTH/2.0f, 15.0f);
		}
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 15));
		g.drawString(currentFrame + "/" + totalFrames, 3.0f*(float)WIDTH/4.0f, 15.0f);
		
		g.setColor(Color.blue);
		Double r = new Rectangle2D.Double(-2.0,-2.0, 1.0, 1.0);
		Shape transformed = at.createTransformedShape(r);
		g.fill(transformed);
	}
	
	private void setFrame()
	{
		if(lastTime < 0)
		{
			lastTime = System.currentTimeMillis()/1000.0;
		}
		
		if(System.currentTimeMillis()/1000.0 - lastTime > frameRate)
		{
			lastTime = System.currentTimeMillis()/1000.0;
			if(playing)
				currentFrame++;
		}
		
		if(currentFrame >= totalFrames)
			currentFrame = 0;
	}

}
