package com.sfm;

import com.sfm.ds.Vector2d;

public class Test {

	public static void main(String[] args)
	{
		getNorm(new Vector2d(1,1),new Vector2d(-1,-1),new Vector2d(1,-1));
		getNorm(new Vector2d(-1,-1),new Vector2d(1,1),new Vector2d(1,-1));
		System.out.println();
	}
	
	public static Vector2d getNorm(Vector2d focal1, Vector2d focal2, Vector2d pos)
	{
		Vector2d e = new Vector2d(1,0);
		Vector2d centre = focal1.add(focal2).mul(0.5);
		double angleOffCentre = Math.acos(focal1.sub(focal2).dot(e)/(focal1.sub(focal2).mag()));
		
		focal1 = focal1.sub(centre).rotate(angleOffCentre);
		focal2 = focal2.sub(centre).rotate(angleOffCentre);
		pos = pos.sub(centre).rotate(angleOffCentre);
		
		System.out.println(centre);
		System.out.println(pos);
		
		
		double a = pos.sub(focal1).add(pos.sub(focal2)).mag();
		System.out.println("a: " + a);
		
		double b;
		if(Math.abs(pos.x) < 0.0000001)
			b = pos.mag();
		else
			b = a*Math.abs(pos.x)/Math.sqrt(a*a - pos.y*pos.y);
		
		System.out.println("b: " + b);
		
		Vector2d norm = new Vector2d(pos.x*b/a,pos.y*b/a).rotate(-angleOffCentre).norm();
		
		System.out.println("norm: " + norm);
		
		/*
		Vector2d a = focal2;
		Vector2d c = pos;
		Vector2d b = focal1;
		Vector2d e = new Vector2d(1,0);
		
		Vector2d cb = b.sub(c);
		Vector2d ca = a.sub(c);
		
		double theta1 = Math.acos(cb.dot(ca)/(cb.mag()* ca.mag()));
		double theta2 = (Math.PI - theta1)/2.0;
		double theta3 = Math.acos(ca.dot(e)/ca.mag());
		double theta4 = theta1 + theta2 - theta3;
		
		Vector2d n = e.rotate(theta4 + (Math.PI/2));
		return n;*/
		return new Vector2d();
	}

}
