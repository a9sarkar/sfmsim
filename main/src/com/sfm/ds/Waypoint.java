package com.sfm.ds;

public interface Waypoint {
	public boolean atWaypoint(Vector2d position);
	public Vector2d getTarget(Vector2d position);
}
