package com.sfm.ds;

public class Ray {
	public Vector2d start;
	public Vector2d direction;
	
	public Ray(Vector2d start, Vector2d direction)
	{
		this.start = start;
		this.direction = direction.norm();
	}
	
	public Vector2d intersect(Ray other)
	{
		//shorten names to simplify equations
		Vector2d as = start;
		Vector2d bs = other.start;
		Vector2d ad = direction;
		Vector2d bd = other.direction;
		/*
		 * solve this set of equations:
		 * p = as + ad * u
		 * p = bs + bd * v
		 * (p is the point of intersection)
		 */
		
		double u = (as.y*bd.x + bd.y*bs.x - bs.y*bd.x - bd.y*as.x ) / (ad.x*bd.y - ad.y*bd.x);
		double v = (as.x + ad.x * u - bs.x) / bd.x;
		
		if(u < 0.0 || v < 0.0)
			return null;
		
		//p = as + ad * u
		Vector2d p = ad.mul(u).add(as);
		
		return p;
	}
}
