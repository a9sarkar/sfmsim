package com.sfm.ds;

import com.sfm.constants.GeneralConst;

public class PointWaypoint implements Waypoint {

	Vector2d point;
	
	public PointWaypoint(Vector2d point)
	{
		this.point = point;
	}
	
	@Override
	public boolean atWaypoint(Vector2d position) {
		return position.sub(point).mag() < GeneralConst.distanceThresh;
	}

	@Override
	public Vector2d getTarget(Vector2d position) {
		return point;
	}
	
	public String toString()
	{
		return point.toString();
	}

}
