package com.sfm.ds;

public class PedestrianBehaviour {
	/* *************requires calibration****************/
	/* *****observable********/
	public NormalDistribution evasiveSpeedChange;
	public NormalDistribution evasiveAngleChange;
	public double accelerationTime;
	public double desiredSpeed;
	/* ****not observable****/
	public double repulsiveStrengthCoefA;
	public double repulsiveStrengthCoefB;
	public double crosswalkPullStrengthCoefA;
	public double crosswalkPullStrengthCoefB;
	public double crosswalkPushStrengthCoefA;
	public double crosswalkPushStrengthCoefB;
	/* ************doesn't require calibration**********/
	public double privateSphere;
	public double evasiveRelativeTimeThresh;
	public double visualRange;
	public double fieldOfView;
}
