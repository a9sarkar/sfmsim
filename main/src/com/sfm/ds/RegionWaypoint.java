package com.sfm.ds;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import com.sfm.terrain.Region;

public class RegionWaypoint implements Waypoint {

	public Region target;
	
	public RegionWaypoint(Region target)
	{
		this.target = target;
	}
	
	@Override
	public boolean atWaypoint(Vector2d position) {
		return target.in(position);
	}

	@Override
	public Vector2d getTarget(final Vector2d position) {
		ArrayList<Line> lines = new ArrayList<Line>();
		
		lines.add(new Line(target.bottomLeft.x,target.bottomLeft.y,target.bottomLeft.x + target.width, target.bottomLeft.y)); //bottom line
		lines.add(new Line(target.bottomLeft.x,target.bottomLeft.y,target.bottomLeft.x, target.bottomLeft.y + target.height)); // left line
		lines.add(new Line(target.bottomLeft.x, target.bottomLeft.y + target.height,target.bottomLeft.x + target.width, target.bottomLeft.y + target.height)); //top line
		lines.add(new Line(target.bottomLeft.x + target.width,target.bottomLeft.y,target.bottomLeft.x + target.width, target.bottomLeft.y + target.height)); //right line
		
		ArrayList<Vector2d> projections = new ArrayList<Vector2d>();
		
		for(Line l : lines)
		{
			projections.add(l.project(position));
		}
		
		Collections.sort(projections,new Comparator<Vector2d>(){

			@Override
			public int compare(Vector2d arg0, Vector2d arg1) {
				double mag0 = arg0.sub(position).mag();
				double mag1 = arg1.sub(position).mag();
				if(mag0 < mag1)
					return -1;
				if(mag0 > mag1)
					return 1;
				return 0;
			}
		});
		
		return projections.get(0);
	}
	
	public String toString()
	{
		return target.toString();
	}

}
