package com.sfm.ds;

import com.sfm.helper.RandomHelper;

public class NormalDistribution {
	public double mu;
	public double sigma;
	
	public NormalDistribution(double mu, double sigma)
	{
		this.mu = mu;
		this.sigma = sigma;
	}
	
	public double nextDouble()
	{
		return RandomHelper.nextGaussian(mu, sigma);
	}
	
	public double nextDouble(double lowerBound, double upperBound)
	{
		return RandomHelper.nextGaussian(mu, sigma, lowerBound, upperBound);
	}
	
}
