package com.sfm.ds;

public class Coordinate {
	
	public Coordinate(Double x, Double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Double x,y;

	@Override
	public String toString() {
		return "Coordinate [x=" + x + ", y=" + y + "]";
	}

}
