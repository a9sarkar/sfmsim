package com.sfm.ds;

public class Line {
	public Vector2d start;
	public Vector2d end;
	
	public Line(Vector2d start, Vector2d end)
	{
		//just for a common ordering
		if(start.x + start.y > end.x + end.y)
		{
			this.start = new Vector2d(end);
			this.end = new Vector2d(start);
		}
		else
		{
			this.start = new Vector2d(start);
			this.end = new Vector2d(end);
		}
	}
	
	public Line(double xStart, double yStart, double xEnd, double yEnd)
	{
		this(new Vector2d(xStart, yStart),new Vector2d(xEnd, yEnd));
	}
	
	public Line(Line edge) {
		this(edge.start, edge.end);
	}

	public boolean vertical()
	{
		return start.x == end.x;
	}
	
	public boolean horizontal()
	{
		return start.y == end.y;
	}
	/**
	 * project a point onto the line. if the projection would be outside the line segment, then the projection is the closest end point
	 * @param point point to project
	 * @return point after projection
	 */
	public Vector2d project(Vector2d point)
	{
		double mag2 = start.sub(end).magSq();
		Vector2d w = end;
		Vector2d v = start;
		Vector2d p = point;
		
		double t = p.sub(v).dot(w.sub(v))/mag2;
		if(t < 0.0)
			return new Vector2d(start);
		if(t > 1.0)
			return new Vector2d(end);
		
		return w.sub(v).mul(t).add(v);
	}
	
	public String toString()
	{
		return start.toString() + " - " + end.toString();
	}
}
