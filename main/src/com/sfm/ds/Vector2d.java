package com.sfm.ds;

public class Vector2d {
	
	public double x;
	public double y;

	public Vector2d(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Initialize x and y to 0
	 */
	public Vector2d()
	{
		x = 0;
		y = 0;
	}
	
	public Vector2d(Vector2d other) {
		this.x = other.x;
		this.y = other.y;
	}

	/**
	 * calculate the magnitude of the vector
	 * @return magnitude of this vector
	 */
	public double mag()
	{
		return Math.sqrt((x*x) + (y*y));
	}
	
	/**
	 * calculate the magnitude squared of the vector
	 * @return magnitude squared of vector
	 */
	public double magSq()
	{
		return x*x + y*y;
	}
	
	/**
	 * <code>this + other</code>
	 * @param other other vector to add
	 * @return <code>this + other</code>
	 */
	public Vector2d add(Vector2d other)
	{
		return new Vector2d(x+other.x,y+other.y);
	}
	
	/**
	 * <code>this - other</code>
	 * @param other other vector to subtract
	 * @return <code>this - other</code>
	 */
	public Vector2d sub(Vector2d other)
	{
		return new Vector2d(x-other.x,y-other.y);
	}
	
	/**
	 * <code>this / v</code>
	 * @param v value to divide by
	 * @return resulting vector
	 */
	public Vector2d div(double v)
	{
		return new Vector2d(x/v,y/v);
	}
	
	public Vector2d mul(double v)
	{
		return new Vector2d(x*v,y*v);
	}
	
	/**
	 * normalize the vector (does not modify <code>this</code> object).
	 * @return normalized vector
	 */
	public Vector2d norm()
	{
		return div(mag());
	}
	
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}

	public double dot(Vector2d other) {
		return x*other.x + y*other.y;
	}
	
	/**
	 * Rotate vector counterclockwise
	 * @param theta angle to rotate vector
	 * @return rotated vector
	 */
	public Vector2d rotate(double theta)
	{
		double c = Math.cos(theta);
		double s = Math.sin(theta);
		return new Vector2d(x*c - y*s, y*c + x*s);
	}

	public double distance(Vector2d other) {
		return sub(other).mag();
	}

	public double angle(Vector2d other) {
		return Math.acos(dot(other)/(mag()*other.mag()));
	}

	public Vector2d add(double val) {
		return new Vector2d(x+val,y+val);
	}
}
