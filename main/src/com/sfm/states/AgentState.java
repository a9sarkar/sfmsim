package com.sfm.states;

import java.util.LinkedList;
import java.util.Queue;

import com.sfm.ds.*;
import com.sfm.terrain.Region;

public class AgentState {
	
	public AgentState(Vector2d position) {
		this.position = position;
		velocity = new Vector2d();
		force = new Vector2d();
		destinations = new LinkedList<Waypoint>();
	}

	public AgentState(AgentState state) {
		position = new Vector2d(state.position);
		velocity = new Vector2d(state.velocity);
		force = new Vector2d(state.force);
		destinations = new LinkedList<Waypoint>(state.destinations);
	}

	public Vector2d position;
	public Queue<Waypoint> destinations;
	public Region curretTerrain;
	public Vector2d velocity;
	public Vector2d force;

	public Vector2d getPosition() {
		return position;
	}

	public void setPosition(Vector2d position) {
		this.position = position;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Position: " + position.toString() + "\n");
		sb.append("Velocity: " + velocity.toString() + "\n");
		sb.append("Force: " + force.toString() + "\n");
		sb.append("destination: " + destinations.peek().toString() + "\n");
		return sb.toString();
	}

}
