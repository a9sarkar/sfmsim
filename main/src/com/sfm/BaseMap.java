package com.sfm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.sfm.ds.Line;
import com.sfm.ds.Vector2d;
import com.sfm.helper.XMLHelper;
import com.sfm.terrain.Crosswalk;
import com.sfm.terrain.Region;
import com.sfm.terrain.Sidewalk;
import com.sfm.terrain.TerrainType;

public class BaseMap {
	
	XMLHelper xmlHelper;
	boolean initialized = false;
	
	public List<Region> spawn; //one side of the road
	
	/** 
	 * terrain consists of crosswalks sidewalks etc.
	 */
	public List<Region> terrain;
	
	public BaseMap(String map)
	{
		xmlHelper = new XMLHelper(map);
		spawn = new ArrayList<Region>();
		terrain = new ArrayList<Region>();
	}

	private Region toSpawn(Element e)
	{
		double x = Double.parseDouble(e.getAttribute("x"));
		double y = Double.parseDouble(e.getAttribute("y"));
		double width = Double.parseDouble(e.getAttribute("width"));
		double height = Double.parseDouble(e.getAttribute("height"));
		return new Region(new Vector2d(x,y),width,height,TerrainType.Spawn);
	}
	
	private Crosswalk toCrosswalk(Element e)
	{
		NodeList children = e.getElementsByTagName("line");
		if(children.getLength() != 2)
			throw new RuntimeException("Crosswalk requires exactly two lines");
		return new Crosswalk(toLine((Element) children.item(0)),toLine((Element) children.item(1)));
	}
	
	private Line toLine(Element e)
	{
		double sx = Double.parseDouble(e.getAttribute("sx"));
		double sy = Double.parseDouble(e.getAttribute("sy"));
		double ex = Double.parseDouble(e.getAttribute("ex"));
		double ey = Double.parseDouble(e.getAttribute("ey"));
		return new Line(sx,sy,ex,ey);
	}
	
	private Sidewalk toSidewalk(Element e)
	{
		NodeList children = e.getElementsByTagName("line");
		if(children.getLength() != 2)
			throw new RuntimeException("Sidewalk requires exactly two lines");
		return new Sidewalk(toLine((Element) children.item(0)),toLine((Element) children.item(1)));
	}
	
	public void init() {
		if(initialized)
			return;
		initialized = true;
		Hashtable<Integer,Region> regions = getRegions();
		
		NodeList edges = xmlHelper.executeXPath("/map/edges/edge");
		
		for(int i = 0; i < edges.getLength(); i++)
		{
			Element cur = (Element) edges.item(i);
			int from = Integer.parseInt(cur.getAttribute("n1"));
			int to = Integer.parseInt(cur.getAttribute("n2"));
			Region n1 = regions.get(from);
			Region n2 = regions.get(to);
			n1.connections.add(n2);
			n2.connections.add(n1);
		}
		
		for(int k : regions.keySet())
		{
			Region cur = regions.get(k);
			terrain.add(cur);
		}
		
		/*Region r1 = new Region(new Vector2d(-10.0, 0.0), 10.0, 2.0);
		Region r2 = new Region(new Vector2d(23.0, 0.0), 10.0, 2.0);
		Region r3 = new Region(new Vector2d(-10.0, 6.0), 10.0, 2.0);
		Region r4 = new Region(new Vector2d(23.0, 6.0), 10.0, 2.0);
		spawn = new ArrayList<Region>();
		spawn.add(r1);
		spawn.add(r2);
		spawn.add(r3);
		spawn.add(r4);
		
		terrain = new ArrayList<Region>();
		
		crosswalk = new Crosswalk(new Line(10.0, 2.0, 10.0, 6.0), new Line(13.0, 2.0, 13.0, 6.0));
		terrain.add(crosswalk);
		terrain.add(new Sidewalk(0.0,0.0,23.0,2.0));
		terrain.add(new Sidewalk(0.0,6.0,23.0,2.0));*/
	}
	
	private Hashtable<Integer,Region> getRegions() {
		NodeList spawns = xmlHelper.executeXPath("/map/nodes/spawn");
		NodeList crosswalks = xmlHelper.executeXPath("/map/nodes/crosswalk");
		NodeList sidewalks = xmlHelper.executeXPath("/map/nodes/sidewalk");
		
		Hashtable<Integer,Region> regions = new Hashtable<Integer,Region>();
		
		for(int i = 0; i < spawns.getLength(); i++)
		{
			Element cur = (Element) spawns.item(i);
			int id = Integer.parseInt(cur.getAttribute("id"));
			Region spawn = toSpawn(cur);
			this.spawn.add(spawn);
			regions.put(id, spawn);
		}
		
		for(int i = 0; i < crosswalks.getLength(); i++)
		{
			Element cur = (Element) crosswalks.item(i);
			int id = Integer.parseInt(cur.getAttribute("id"));
			regions.put(id, toCrosswalk(cur));
		}
		
		for(int i = 0; i < sidewalks.getLength(); i++)
		{
			Element cur = (Element) sidewalks.item(i);
			int id = Integer.parseInt(cur.getAttribute("id"));
			regions.put(id, toSidewalk(cur));
		}
		return regions;
	}

}
