package com.sfm.ignore;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.nonstiff.EulerIntegrator;


class MotionODE implements FirstOrderDifferentialEquations {

    private double f;

    public MotionODE(double f) {
        this.f = f;
    }

    public int getDimension() {
        return 2;
    }

    public void computeDerivatives(double t, double[] y, double[] yDot) {
        yDot[0] = y[1] + f*t ;
        yDot[1] = f ;
    }
    
    public static void main(String[] args) {
    	FirstOrderIntegrator euI = new EulerIntegrator(1.0);
    	FirstOrderDifferentialEquations ode = new MotionODE(5);
    	double[] y = new double[] { 10.0, 2.0 };
    	euI.integrate(ode, 0.0, y, 2.0, y);
    	System.out.println(String.valueOf(y[0]) + " " + String.valueOf(y[1]));
    }

}