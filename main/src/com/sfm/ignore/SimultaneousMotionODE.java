package com.sfm.ignore;

import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.FirstOrderIntegrator;
import org.apache.commons.math3.ode.nonstiff.EulerIntegrator;

public class SimultaneousMotionODE implements FirstOrderDifferentialEquations {

    
    private double k;

	public SimultaneousMotionODE(double k) {
        this.k = k; 
    }

    public int getDimension() {
        return 4;
    }

    public void computeDerivatives(double t, double[] y, double[] yDot) {
        yDot[0] = y[2] + k * Math.abs(y[0] - y[1]) * t ;
        yDot[1] = - y[3] - k * Math.abs(y[0] - y[1]) *t ;
        yDot[2] = k * Math.abs(y[0] - y[1]) * t;
        yDot[3] = - k * Math.abs(y[0] - y[1]) * t;
        //System.out.println(t);
    }
    
    public static void main(String[] args) {
    	FirstOrderIntegrator euI = new EulerIntegrator(0.01);
    	FirstOrderDifferentialEquations ode = new SimultaneousMotionODE(5);
    	double[] y = new double[] { 0.0, 100.0, 5.0 , 2.0 };
    	euI.integrate(ode, 0.0, y, 0.5, y);
    	System.out.println(String.valueOf(y[0]) + " " + String.valueOf(y[1]));
    	System.out.println(String.valueOf(y[2]) + " " + String.valueOf(y[3]));
    	euI.integrate(ode, 0.5, y, 1, y);
    	System.out.println(String.valueOf(y[0]) + " " + String.valueOf(y[1]));
    	System.out.println(String.valueOf(y[2]) + " " + String.valueOf(y[3]));
    }

}
