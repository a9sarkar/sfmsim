package com.sfm.agents.factories;

import com.sfm.BaseMap;
import com.sfm.agents.AgentContainer;
import com.sfm.agents.CarFactory;
import com.sfm.agents.EgoCar;
import com.sfm.agents.Pedestrian;
import com.sfm.agents.PedestrianFactory;

public class SceneFactory {
	public static AgentContainer generateScene(int numPed, BaseMap map)
	{
		map.init();
		
		AgentContainer agents = new AgentContainer();
		
		for(int i = 0; i < numPed; i++)
		{
			Pedestrian p = PedestrianFactory.generatePedestrian(map);
			agents.getInternalList().add(p);
			//System.out.println(p.toString() + "\n");
		}
		
		EgoCar car = CarFactory.generateEgoCar();
		agents.setEgoCar(car);
		return agents;
	}
}
