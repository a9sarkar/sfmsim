package com.sfm.agents;

import java.util.List;

import com.sfm.constants.GeneralConst;
import com.sfm.ds.Vector2d;
import com.sfm.states.AgentState;;

public class Group extends Agent {

	private List<Agent> members;
	
	public Group(AgentState state, List<Agent> members)
	{
		super(state,AgentType.Group);
		this.members = members;
	}
	
	@Override
	public Vector2d getForce(Pedestrian other, double dt) {
		Vector2d force = new Vector2d();
		for(int i = 0; i < members.size(); i++)
		{
			
		}
		
		
		return null;
	}

	@Override
	public Vector2d getDriveForce() {
		return new Vector2d();
	}

	/*@Override
	public double distanceToTarget() {
		for(int i = 0; i < members.size(); i++)
		{
			double dist = members.get(i).distanceToTarget();
			if ( dist <= GeneralConst.distanceThresh)
				return dist;
		}
		return Double.POSITIVE_INFINITY;
	}*/

	@Override
	public void updateFromPrimitiveList(double[] y, int ind) {
		
	}

	@Override
	public boolean atTarget() {
		return false;
	}

	@Override
	public Vector2d getTerrainForce() {
		return null;
	}

	@Override
	public boolean inNeighbourhood(Agent other) {
		return false;
	}

}
