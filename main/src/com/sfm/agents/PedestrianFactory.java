package com.sfm.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.sfm.ds.NormalDistribution;
import com.sfm.ds.PedestrianBehaviour;
import com.sfm.ds.PointWaypoint;
import com.sfm.ds.RegionWaypoint;
import com.sfm.BaseMap;
import com.sfm.constants.PedestrianConst;
import com.sfm.ds.Vector2d;
import com.sfm.ds.Waypoint;
import com.sfm.helper.RandomHelper;
import com.sfm.states.AgentState;
import com.sfm.terrain.Region;


public class PedestrianFactory {
	
	public static Pedestrian generatePedestrian(BaseMap map)
	{
		int spawnSize = map.spawn.size();
		int spawnInd = ThreadLocalRandom.current().nextInt(spawnSize);
		Region spawn = map.spawn.get(spawnInd);
		int finishInd;
		do
		{
			finishInd = RandomHelper.nextInt(map.spawn.size());
		} while(finishInd == spawnInd);
		
		Region finish = map.spawn.get(finishInd);
		
		Vector2d start = RandomHelper.nextVector2d(spawn);
		
		AgentState state = new AgentState(start);
		
		state.destinations.addAll(generateWaypoints(spawn,finish));
		
		PedestrianBehaviour pb = new PedestrianBehaviour();
		
		pb.repulsiveStrengthCoefA = PedestrianConst.repulsiveStrengthCoefA;
		pb.repulsiveStrengthCoefB = PedestrianConst.repulsiveStrengthCoefB;
		pb.privateSphere = PedestrianConst.privateSphere;
		pb.evasiveRelativeTimeThresh = PedestrianConst.evasiveRelativeTimeThresh;
		pb.evasiveSpeedChange = PedestrianConst.evasiveSpeedChange;
		pb.evasiveAngleChange = PedestrianConst.evasiveAngleChange;
		pb.visualRange = PedestrianConst.visualRange;
		pb.fieldOfView = PedestrianConst.fieldOfView;
		pb.crosswalkPullStrengthCoefA = PedestrianConst.crosswalkPullStrengthCoefA;
		pb.crosswalkPullStrengthCoefB = PedestrianConst.crosswalkPullStrengthCoefB;
		pb.crosswalkPushStrengthCoefA = PedestrianConst.crosswalkPushStrengthCoefA;
		pb.crosswalkPushStrengthCoefB = PedestrianConst.crosswalkPushStrengthCoefB;
		pb.accelerationTime = PedestrianConst.accelerationTime.nextDouble(0.001,2.0); //set small value as lowerbound
		pb.desiredSpeed = PedestrianConst.desiredSpeed.nextDouble(0.001,200.0);
		
		Pedestrian p = new Pedestrian(state,pb);
		p.terrain = map.terrain;
		p.state.curretTerrain = spawn;
		
		return p;
	}
	
	private static List<Waypoint> generateWaypoints(Region start, Region end)
	{
		//naive breadth first search
		//TODO: implement better search
		
		ArrayList<Region> visited = new ArrayList<Region>();
		Queue<Region> toVisit = new LinkedList<Region>();
		HashMap<Region, Region> paths = new HashMap<Region, Region>();
		
		toVisit.add(start);
		
		while(!toVisit.isEmpty())
		{
			Region cur = toVisit.remove();
			visited.add(cur);
			
			if(cur == end)
			{
				break;
			}
			
			for(Region r : cur.connections)
			{
				if(!visited.contains(r))
				{
					paths.put(r, cur);
					toVisit.add(r);
				}
			}
			
		}
		
		ArrayList<Waypoint> waypoints = new ArrayList<Waypoint>();
		Region cur = end;
		
		waypoints.add(new RegionWaypoint(cur));
		
		while(cur != start)
		{
			cur = paths.get(cur);
			if(cur != start)
				waypoints.add(0, new RegionWaypoint(cur));
		}
		
		return waypoints;
	}
}
