package com.sfm.agents;

import com.sfm.states.AgentState;
import com.sfm.terrain.Region;

import java.util.List;

import com.sfm.constants.PedestrianConst;
import com.sfm.ds.*;

public class Pedestrian extends Agent {

	public Pedestrian(AgentState state, PedestrianBehaviour behaviour) {
		super(state,AgentType.Pedestrian);
		this.behaviour = behaviour;
	}
	
	public PedestrianBehaviour behaviour;
	
	public List<Region> terrain;
	
	@Override
	public Vector2d getForce(Pedestrian other, double dt)
	{
		if(state.velocity.mag() == 0 || other.state.velocity.mag() == 0)
			return new Vector2d();
		
		Vector2d evasion = getEvasive(other,dt);
		Vector2d repulsive = getRepulsive(other,dt);
		
		return repulsive.add(evasion);
	}
	
	private Vector2d getRepulsive(Pedestrian other, double dt)
	{
		Vector2d focal1 = state.position;
		Vector2d focal2 = state.position.add(other.state.velocity.mul(dt));
		Vector2d pos = other.state.position;
		
		Vector2d e = new Vector2d(1,0);
		Vector2d centre = focal1.add(focal2).mul(0.5);
		double angleOffCentre = Math.acos(focal1.sub(focal2).dot(e)/(focal1.sub(focal2).mag()));
		
		focal1 = focal1.sub(centre).rotate(angleOffCentre);
		focal2 = focal2.sub(centre).rotate(angleOffCentre);
		pos = pos.sub(centre).rotate(angleOffCentre);
		
		Vector2d dist = pos.sub(focal1);
		Vector2d posUpdate = focal2;
		
		double a = pos.sub(focal1).add(pos.sub(focal2)).mag();
		
		double b;
		if(Math.abs(pos.x) < 0.0000001)
			b = pos.mag();
		else
			b = a*Math.abs(pos.x)/Math.sqrt(a*a - pos.y*pos.y);
		
		Vector2d n = new Vector2d(pos.x*b/a,pos.y*b/a).rotate(-angleOffCentre).norm();
		
		/*Vector2d dist = this.state.position.sub(other.state.position);
		Vector2d posUpdate = other.state.velocity.mul(dt);
		
		double inner = Math.sqrt(Math.pow(dist.mag() + dist.sub(posUpdate).mag(), 2) - Math.pow(posUpdate.mag(), 2))/2;
	
		Vector2d a = other.state.position.add(other.state.velocity.mul(dt));
		Vector2d c = this.state.position;
		Vector2d b = other.state.position;
		Vector2d e = new Vector2d(1,0);
		
		Vector2d cb = b.sub(c);
		Vector2d ca = a.sub(c);
		
		double theta1 = Math.acos(cb.dot(ca)/(cb.mag()* ca.mag()));
		double theta2 = (Math.PI - theta1)/2.0;
		double theta3 = Math.acos(ca.dot(e)/ca.mag());
		double theta4 = theta1 + theta2 - theta3;
		
		Vector2d n = e.rotate(theta4 + (Math.PI/2));
		Vector2d test = e.rotate(Math.PI/2);*/
		
		//double temp = this.state.position.sub(other.state.position).mag();
		
		Vector2d repulsion = n.mul(behaviour.repulsiveStrengthCoefA*Math.exp(-behaviour.repulsiveStrengthCoefB*b));
		
		return repulsion;
	}
	
	private Vector2d getEvasive(Pedestrian other, double dt)
	{
		if(state.position.sub(other.state.position).mag() > behaviour.privateSphere)
			return new Vector2d();
		
		double relativeTime;
		
		AgentState state = new AgentState(this.state);
		do
		{
			Ray myRay = new Ray(state.position,state.velocity);
			Ray otherRay = new Ray(other.state.position,other.state.velocity);
			
			Vector2d collisionPoint = myRay.intersect(otherRay);
			
			if(collisionPoint == null)
				return new Vector2d();
			
			//time to conflict point
			Vector2d toCollisionA = collisionPoint.sub(state.position);
			Vector2d toCollisionB = collisionPoint.sub(other.state.position);
			double TTCPa = (toCollisionA.mag()/state.velocity.mag())*Math.cos(state.velocity.dot(toCollisionA));
			double TTCPb = (toCollisionB.mag()/other.state.velocity.mag())*Math.cos(other.state.velocity.dot(toCollisionB));
			
			if(TTCPa > 0 && TTCPb > 0)
				relativeTime = Math.abs(TTCPa - TTCPb);
			else
				relativeTime = Double.POSITIVE_INFINITY;
			
			if(relativeTime < behaviour.evasiveRelativeTimeThresh)
			{
				double speedChange = behaviour.evasiveSpeedChange.nextDouble(-2.0,2.0)*dt;
				double angleChange = behaviour.evasiveAngleChange.nextDouble()*(Math.PI/180.0)*dt;
				
				//System.out.println("Speed Change: " + speedChange + " angle change: " + angleChange);
				
				state.velocity = state.velocity.rotate(angleChange).mul(1 + speedChange);
			}
		} while(relativeTime < behaviour.evasiveRelativeTimeThresh);
		
		Vector2d evasion = state.velocity.sub(this.state.velocity).mul(1/dt);
		return evasion;
	}
	
	@Override
	public Vector2d getDriveForce() {
		Waypoint curDest = state.destinations.peek();
		
		Vector2d desiredDirection = curDest.getTarget(state.position).sub(state.position).norm();
		
		//F = (1/accTime) * (dSpeed * desDir - curVel)
		Vector2d Force = desiredDirection.mul(behaviour.desiredSpeed).sub(state.velocity).mul(1.0/behaviour.accelerationTime);
		
		return Force;
	}
	
	@Override
	public Vector2d getTerrainForce()
	{
		return state.curretTerrain.getForce(this);
		/*Vector2d force = new Vector2d();
		for(Region r : terrain)
		{
			force = force.add(r.getForce(this));
		}
		
		return force;*/
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Pedestrian:\n" + state.toString());
		sb.append("desiredSpeed: " + behaviour.desiredSpeed + "\n");
		sb.append("accelerationTime: " + behaviour.accelerationTime + "\n");
		return sb.toString();
	}

	@Override
	public boolean atTarget() {
		boolean ret = state.destinations.peek().atWaypoint(state.position);
		if(ret)
		{
			RegionWaypoint at = (RegionWaypoint) state.destinations.poll();
			state.curretTerrain = at.target;
		}
		return ret;
	}

	@Override
	public void updateFromPrimitiveList(double[] y, int ind) {
		state.position.x = y[ind];
		state.velocity.x = y[ind+1];
		state.position.y = y[ind+2];
		state.velocity.y = y[ind+3];
	}

	@Override
	public boolean inNeighbourhood(Agent other) {
		boolean visualRange = state.position.sub(other.state.position).mag() < behaviour.visualRange;
		boolean fov = other.state.position.sub(state.position).angle(state.velocity) < behaviour.fieldOfView;
		return visualRange && fov;
	}
}
