package com.sfm.agents;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import com.sfm.constants.EgoCarConst;
import com.sfm.ds.Vector2d;
import com.sfm.states.AgentState;

public class EgoCar extends Agent {
	
	private boolean going;
	
	public EgoCar(AgentState state) {
		super(state, AgentType.EgoCar);
		going = false;
	}
	
	public void setGoing(boolean going)
	{
		this.going = going;
	}
	
	public void toggleGoing()
	{
		going = !going;
	}
	
	@Override
	public Vector2d getForce(Pedestrian other, double dt) {
		return new Vector2d();
	}

	@Override
	public Vector2d getDriveForce() {
		if(!going)
		{
			if(state.velocity.x > 0)
				return new Vector2d(EgoCarConst.stoppingForce,0);
		}
		else
		{
			if(state.velocity.x < 5.5) //~ 20kph
				return new Vector2d(EgoCarConst.drivingForce,0);
		}
		return new Vector2d();
	}

	@Override
	public boolean atTarget() {
		return state.position.x > EgoCarConst.maxX;
	}

	@Override
	public void updateFromPrimitiveList(double[] y, int ind) {
		state.position.x = y[ind];
		state.velocity.x = y[ind+1];
		state.position.y = y[ind+2];
		state.velocity.y = y[ind+3];
	}

	@Override
	public Vector2d getTerrainForce() {
		return new Vector2d();
	}
	
	public boolean hit(Pedestrian other)
	{
		Rectangle2D.Double rect = new  Rectangle2D.Double(state.position.x - (EgoCarConst.carLength/2.0),
				state.position.y - (EgoCarConst.carWidth/2.0),
				EgoCarConst.carLength,
				EgoCarConst.carWidth);
		Ellipse2D.Double e = new Ellipse2D.Double(other.state.position.x, other.state.position.y, 0.5, 0.5);
		return e.intersects(rect);
	}

	public boolean getGoing() {
		return going;
	}

	@Override
	public boolean inNeighbourhood(Agent other) {
		return false;
	}
}
