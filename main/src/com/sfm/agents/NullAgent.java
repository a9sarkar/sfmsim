package com.sfm.agents;

import com.sfm.ds.Vector2d;
import com.sfm.states.AgentState;

public class NullAgent extends Agent {

	public NullAgent(AgentState state) {
		super(state, AgentType.Null);
	}

	@Override
	public Vector2d getForce(Pedestrian other, double dt) {
		return new Vector2d();
	}

	@Override
	public Vector2d getDriveForce() {
		return new Vector2d();
	}

	@Override
	public void updateFromPrimitiveList(double[] y, int ind) {
		//do nothing
	}

	@Override
	public boolean atTarget() {
		return false;
	}

	@Override
	public Vector2d getTerrainForce() {
		return new Vector2d();
	}

	@Override
	public boolean inNeighbourhood(Agent other) {
		return false;
	}

}
