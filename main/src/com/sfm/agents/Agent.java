package com.sfm.agents;


import com.sfm.states.AgentState;
import com.sfm.ds.*;

public abstract class Agent {
	
	public Agent(AgentState state,AgentType type) {
		this.type = type;
		this.state = state;
	}
	
	AgentType type;
	
	public AgentType getType() {
		return type;
	}

	public void setType(AgentType type) {
		this.type = type;
	}

	AgentState state;
	
	public AgentState getState() {
		return state;
	}

	public void setState(AgentState state) {
		this.state = state;
	}
	
	/**
	 * Get force that <code>this</code> applies to <code>other</code>
	 * @param other
	 * @return vector force applied
	 */
	public abstract Vector2d getForce(Pedestrian other, double dt);
	
	/**
	 * Get the driving force for the agent. Typically driving force to destination.
	 * @return vector force
	 */
	public abstract Vector2d getDriveForce();
	
	/**
	 * Determine if the agent is at it's target
	 * @return true if the agent has reached the current target, false otherwise
	 */
	public abstract boolean atTarget();
	
	/**
	 * update the values of this agent based on the primitive list
	 * @param y primitive list
	 * @param ind start index
	 */
	public abstract void updateFromPrimitiveList(double[] y, int ind);
	
	public abstract Vector2d getTerrainForce();
	
	public abstract boolean inNeighbourhood(Agent other);
}
