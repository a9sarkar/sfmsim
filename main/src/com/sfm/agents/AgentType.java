package com.sfm.agents;

public enum AgentType {
	Pedestrian,
	Car,
	EgoCar,
	Byciclist,
	Group,
	Null
}
