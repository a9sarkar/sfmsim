package com.sfm.agents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sfm.constants.EgoCarConst;
import com.sfm.constants.PedestrianConst;
import com.sfm.ds.Vector2d;
import com.sfm.helper.Parallel;
import com.sfm.states.AgentState;

public class AgentContainer implements Iterable<Agent> {
	
	private List<Agent> agentList;
	
	public EgoCar car;
	
	public static final int numParams = 4;
	
	public AgentContainer()
	{
		agentList = new ArrayList<Agent>();
	}
	
	public AgentContainer(List<Agent> agents)
	{
		agentList = agents;
	}
	
	@Override
	public Iterator<Agent> iterator() {
		return agentList.iterator();
	}
	
	public int primitiveLength()
	{
		return agentList.size()*numParams;
	}
	
	public List<Agent> getInternalList()
	{
		return agentList;
	}
	
	public double[] toPrimitiveList()
	{
		double[] y = new double[agentList.size()*numParams];
		for(int i = 0; i < agentList.size(); i++)
		{
			int yInd = numParams*i;
			y[yInd] = agentList.get(i).state.position.x;
			y[yInd+1] = agentList.get(i).state.velocity.x;
			y[yInd+2] = agentList.get(i).state.position.y;
			y[yInd+3] = agentList.get(i).state.velocity.y;
		}
		return y;
	}
	
	public void UpdateFromPrimitiveList(double[] y)
	{
		if(y.length % numParams != 0)
			throw new RuntimeException("input array does not have proper number of params");
		
		for(int i = 0; i < y.length; i += numParams)
		{
			int aInd = (int)((double)i/numParams);
			Agent curAgent = agentList.get(aInd);
			curAgent.updateFromPrimitiveList(y, i);
			
			if(curAgent.atTarget())
			{
				if(curAgent.state.destinations.isEmpty())
					agentList.set(aInd, new NullAgent(new AgentState(curAgent.state.position)));
			}
		}
	}

	public void computeDerivatives(final double dt, final double[] yDot) {
		Collection<Integer> elems = new LinkedList<Integer>();
		for (int i = 0; i < 40; ++i) {
		    elems.add(i);
		}
		
		for(int i = 0; i < agentList.size(); i++)
		//only use parallel for when not running multiple simulations
		//Parallel.For(elems,
				//new Parallel.Operation<Integer>() {
		    //public void perform(Integer i) {
		{
			int yInd = i*numParams;
			
			Agent curAgent = agentList.get(i);
			Vector2d force = curAgent.getDriveForce();
			force = force.add(curAgent.getTerrainForce());
			
			List<Agent> neighbours = getNeighbours(curAgent);
			for(int j = 0; j < neighbours.size(); j++)
			{
				
				Agent other = neighbours.get(j);
				Vector2d toAdd = new Vector2d();
				
				if(curAgent.type == AgentType.Pedestrian)
					toAdd = other.getForce((Pedestrian)curAgent, dt);
				int b = 1;
				
				if(Double.isNaN(toAdd.x)||Double.isNaN(toAdd.y) || Double.isNaN(force.x)||Double.isNaN(force.y))
				{
					int a = 0;
				}
				
				force = force.add(toAdd);
			}
			
			//if(curAgent.type == AgentType.Pedestrian)
			//	System.out.println(curAgent.state.position + " ; " + curAgent.state.velocity + " ; " + force);
			
			yDot[yInd] = curAgent.state.velocity.x + force.x*dt;
			yDot[yInd+1] = force.x;
			yDot[yInd+2] = curAgent.state.velocity.y + force.y*dt;
			yDot[yInd+3] = force.y;
		    }
		//});
	}

	private List<Agent> getNeighbours(Agent curAgent) {
		ArrayList<Agent> ret = new ArrayList<Agent>();
		for(int i = 0; i < agentList.size(); i++)
		{
			Agent other = agentList.get(i);
			if(other == curAgent)
				continue;
			
			if(curAgent.inNeighbourhood(other))
				ret.add(other);
		}
		return ret;
	}

	public void setEgoCar(EgoCar car) {
		this.car = car;
		if(EgoCarConst.includeCar)
			agentList.add(car);
	}
}
