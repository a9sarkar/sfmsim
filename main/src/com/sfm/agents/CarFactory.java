package com.sfm.agents;

import com.sfm.ds.Vector2d;
import com.sfm.states.AgentState;

public class CarFactory {
	public static EgoCar generateEgoCar()
	{
		AgentState state = new AgentState(new Vector2d(-4.0,3.0));
		EgoCar car = new EgoCar(state);
		return car;
	}
}
