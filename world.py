"""a wrapper of the world dynamics"""

import jpype as jp
import numpy as np

NOT_STARTED = 1
NORM_STATE = 2
TERM_STATE = 3

jp.startJVM(jp.getDefaultJVMPath(), "-ea")

class State:
	def __init__(self, data=None, flag=NORM_STATE):
		self.data = data
		self.flag = flag

class World:
	def __init__(self):		
		jp.java.lang.System.out.println("Java print test")
		self.engine = jp.JPackage('com').sfm.engine.SFMEngine(0.1)
		self.state = State()
	def nextstep(self, action):
		assert action == 1 or action == 0
		if self.state.flag == TERM_STATE:
		  reward = 0
		else:
		  reward = self.engine.updateAndStep(jp.JBoolean(action))
		return reward
	def getstate(self):
		self.state = State(data = np.array(self.engine.getState()),flag = self.engine.getLabel())
		return self.state
	def reset(self):
		self.engine = jp.JPackage('com').sfm.engine.SFMEngine(0.1)
	def shutdown():
		jp.shutdownJVM()
	