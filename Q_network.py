"""Q learning with neural network function approximator
   the network input is the state of the environment, and
   the output is the Q value for throttle or not
"""

import numpy as np
import tensorflow as tf
import world as wd
import copy as cp

GAMMA = 1
LEARN_RATE = 0.05
EXPLORATION = 1
NUM_OF_TRIAL = 40000


HEIGHT = 19
WIDTH = 10
DEPTH = 3
x = tf.placeholder(tf.float32, shape=[None, HEIGHT, WIDTH, DEPTH], name="state")

y_ = tf.placeholder(tf.float32, shape=[None, 2], name="temporal_target")

lr = tf.placeholder(tf.float32, shape=[1], name="learning_rate")

h_1 = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')


W_conv1 = tf.Variable(tf.truncated_normal([9, 5, 3, 6], stddev=0.1))
b_conv1 = tf.Variable(tf.constant(0.1, shape=[6]))
h_2 = tf.nn.relu(tf.nn.conv2d(h_1, W_conv1, 
                              strides=[1, 1, 1, 1], padding='VALID')
                + b_conv1)

W_fc1 = tf.Variable(tf.truncated_normal([6, 2], stddev=0.1))
b_fc1 = tf.Variable(tf.constant(0.1, shape=[2]))
y = tf.matmul(tf.reshape(h_2, [-1, 6]), W_fc1) + b_fc1
tf.scalar_summary("Q", y[0][0])

loss = tf.reduce_mean(tf.square(y - y_))
train = tf.train.GradientDescentOptimizer(lr[0]).minimize(loss)

sess = tf.Session()
sess.run(tf.initialize_all_variables())

merged = tf.merge_all_summaries()
writer = tf.train.SummaryWriter('tflog', sess.graph)

world = wd.World()


for trial in range(0, NUM_OF_TRIAL):
	print("trial: ", trial)
	summary = sess.run(merged, feed_dict={x: np.reshape(3*np.ones([HEIGHT, WIDTH, DEPTH]), [1, HEIGHT, WIDTH, DEPTH])})
	writer.add_summary(summary, trial)
	prevstate = wd.State(flag = wd.NOT_STARTED)
	reward = 0
	while prevstate.flag != wd.TERM_STATE:
		currentstate = world.getstate()
		#print(currentstate.flag,prevstate.flag)
		if currentstate.flag != wd.TERM_STATE:
			q = sess.run(y, feed_dict={x: np.reshape(currentstate.data, [1,HEIGHT, WIDTH, DEPTH])})
		else:
			q = np.array([[0.0, 0.0]])

		if prevstate.flag != wd.NOT_STARTED:
			prevq[0][actionindex] = reward+GAMMA*np.max(q)
			sess.run(train, feed_dict={x: np.reshape(prevstate.data, [1, HEIGHT, WIDTH, DEPTH]),
									 lr: [LEARN_RATE/(5000+trial)],
									 y_: prevq})  

		if np.random.binomial(1, EXPLORATION) == 1:
			actionindex = np.random.randint(0, np.size(q))
			EXPLORATION /= 1.2
		else:
			actionindex = np.argmax(q)
		#print('choosing: ', actionindex)
		reward = reward + world.nextstep(actionindex)
		#print('reward: ', reward)
		prevstate = currentstate
		prevq = np.copy(q)
	print("reward: ", reward)
	world.reset()

world.shutdown()