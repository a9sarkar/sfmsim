import tflearn as tf
import world

HEIGHT = 18
WIDTH = 10
DEPTH = 3

network = input_data(shape=[HEIGHT,WIDTH,DEPTH])
network = fully_connected(network,320)
network = dropout(network,0.5)
network = fully_connected(network,320)
network = dropout(network,0.5)
network = fully_connected(network,320)
network = dropout(network,0.5)